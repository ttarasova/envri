#!/usr/bin/python

# This script demonstrates how to use the refine-python library on the sample of the ICOS dataset.

# ------------------ Prerequisites
# operations.json was generated beforehand on one of the input (ICOS) files (from ../data/original/ICOS.tar.gz) using the stand-alone application OpenRefine https://github.com/OpenRefine/OpenRefine
# ----------------------------------------------------------------------

# ------------------ Dependencies
# The refine-python library must be in the same directory (downloadable from https://github.com/OpenRefine/refine-python)
# The operations.json must be in the same directory as the script
# INPUT_DATA must contain input files 
# ----------------------------------------------------------------------

import os, sys
sys.path.append("refine.py")
import refine

INPUT_DATA = "../data/original/ICOS/"
OUTPUT_DATA_FOLDER = "../data/rdf/ICOS/"

# create a refine instance
r = refine.Refine()

# conversion steps
p = r.new_project("MHD.41.co2")
p.apply_operations("operations.json")
print p.export_rows()
p.delete_project()

# TODO: implement iteration over files from INPUT_DATA and applies conversion steps to them

# for root, dirs, filenames in os.walk(INPUT_DATA):
# 	for filename in filenames:
# 		# 1. create new project
# 		p = r.new_project(root+filename)
# 		# 2. apply opertions.json to the new project
# 		p.apply_operations("../resources/icos-mappings.json")
# 		# 3. create an output file
# 		base = os.path.basename(filename)
# 		f_without_ext = os.path.splitext(base)[0]
# 		f_out = open(OUTPUT_DATA_FOLDER+f_without_ext+".rdf", 'w')
# 		# 4. save the export rows into the output file
# 		print >> f_out, p.export_rows()
# 		f_out.close()
# 		# 5. delete the project
# 		p.delete_project()
# 		print "End of processing of ", filename