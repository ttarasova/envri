<!DOCTYPE html>
<html>
<head>
<title> ENVRI: Linked Data Harmonisation Model </title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<style>

html, body {
    height: 100%;
    width: 100%;
}

body {
    position: relative;
}

h1, h2, h3
{
color: #664220;
}

img.workflow {
	max-width: 40%;
    height: auto;
}

img.logo {
	max-width: 70%;
    height: auto;
}

img.diagram {
	max-width: 90%;
    height: auto;
}

div.caption, div.case-study
{
	text-align: center;
	font-style: italic;
	padding: 15px
}

div.definition
{
	border:1px solid;
	padding: 15px;
}


p.code
{
	background-color:#E1B68C;
	padding: 15px;
	border:1px solid;
	border-color: #664220}

p.header
{
	font-weight:bold
}

li.bold
{
	font-weight:bold
}

table.noborder, th.noborder, td.noborder, tr.noborder
{
	border: none;
	border-collapse:collapse;
	padding:0px;
}

table.prefixes
{
	border: none;
	border-collapse:collapse;
	padding:0px;
}

td.logo
{
	width:20%;
	text-align:center;
}

table.bordered
{
	border-collapse:collapse;
	margin-left:auto; 
	margin-right:auto;
}

th 
{
	font-size:0.9em;
	text-align:center;
	padding-top:5px;
	padding-bottom:4px;
	background-color: #E1B68C;
	color:#664220;
}

td, th 
{
	border:1px solid #664220;
	padding:3px 7px 2px 7px;
}
td.centered
{
	text-align:center
}

td.revision
{
	vertical-align: top
}
tr.alt td 
{
	color:#664220;
	background-color: #F0DAC5;
}

th.query
{
	background-color:#F0DAC5;
}

</style>

</head>

<body>

<h1> Linked Data Harmonisation Model</h1>

<table class="noborder">
<tr class="noborder">
<td class="noborder" collspan="2">
<p>This page describes the Linked Data Harmonisation Model for Environmental Data which was developed within the context of the <a href="http://envri.eu/" target="_blank">EU-FP7 project ENVRI</a>. The goal of the component is to enable integration of highly heterogeneous environmental data for further processing and analysis.</p>
 

<p>The <a href="case-study.html" target="_blank">"Iceland Volcano Ash" study case</a> page describes the demonstration of the harmonisation model.</p></td>

<td class="noborder logo"><a href="http://envri.eu/" target="_blank"><img class="logo" src="pics/logo-ENVRI.jpg" alt="ENVRI logo"/></a></td>
</tr>

<tr class="noborder">
<td colspan="2" class="noborder">

<h2>Summary and Outline</h2>

<p>In this work we investigate opportunities that the <a href="http://www.w3.org/DesignIssues/LinkedData.html" target="_blank">Linked Data</a> publishing technology provides for environmental data integration. Linked Data allows for data representation at much finer granularity than document level. Any abstract concept or a real world entity can be represented using the <a href="http://www.w3.org/DesignIssues/LinkedData.html" targte="_blank">principles of Linked Data</a>.
</p>

<p>Environmental data is collections of observations of the Earth environment. An observation typically consists of a time and geospatial location corresponding to the observation and additional information that helps to interprete the observation, e.g., an instrument used to perform the observation, a unit of measure in which the result of the observation is given, etc. The concept of <i>observation</i> is central to environmental data, this suggests an observation-centric model which is introduced in <a href="#model">Section I Environmental Data Model</a>. The model was developed based on the the <i>standard conceptual model of observations and measurements (O&M)</i> <a href="#ref5">[5]</a> endorsed by <a href="http://www.opengeospatial.org/" target="_blank">OGC</a> and <a href="http://www.iso.org/iso/home.html" target="_blank">ISO</a>.</p>

<p>The model was implemented in RDFS in the ENVRI vocabulary, discussed in <a href="#vocab">Section II ENVRI Vocabulary</a>.</p>

<p>We proposed the workflow for harmonising heterogeneous environmental data in <a href="#workflow">Section III Workflow for Environmental Data Harmonisation</a>. We used the ENVRI vocabulary to align semantics of the data, and the <a href="http://www.w3.org/TR/vocab-data-cube/" target="_blank">RDF Data Cube vocabulary</a> to align scheme of the data. The idea of using Data Cube for publishing environmental Linked Data is not new. There is a number of related works available <a href="#ref1">[1]</a>,<a href="#ref2">[2]</a>, <a href="#ref3">[3]</a>. What differs our work from the existing efforts is the usage of the ENVRI vocabulary as an intermediate top-level ontological layer of generic Earth science observation terms.</p>

<p>We present the on-going work on the extension of the model in <a href="#extend-model">Section IV Data Model Extension</a>.</p>

</td></tr></table>



<h2> Table of Content </h2>

<ol type="I">

	<li> <a href="#model">Environmental Data Model</a> </li>

	<li> <a href="#vocab">The ENVRI Vocabulary</a> </li>
	
	<ul>
		<li> <a href="#ontologies">Cross-Domain Ontologies</a> </li>
		<li><a href="#merging">Merging the ENVRI vocabulary with Data Cube</a> </li>
	</ul>

	<li> <a href="#workflow">Workflow for Environmental Data Harmonisation</a> </li>
		<ul>
			<li><a href="#harmonisation">Data Harmonisation Process</a> </li>
		</ul>
	
	<li> <a href="#extend-model">Data Model Extension</a> </li>
		<ul>
			<li><a href="#sampling-feature">Sampling Feature</a></li>
			<li><a href="#types-of-op">Types of Observed Property</a></li>
			<li><a href="#elaboration-on-procedure">Elaboration on Procedure</a></li>
			<li><a href="#result-types">Results Types</a></li>
		</ul>

	<li> <a href="#resources">Resources</a> </li>

	<li> <a href="#references">References</a> </li>
</ol>


<hr />

<h1> <a name="model">I Environmental Data Model</a> </h1>

<h2>Central concepts</h2>

<p>In order to identify common concepts of environmental data, we refer to the <i>ISO and OGC standard conceptual model for observations and measurements</i> <a href="#ref5" target="_blank">[5]</a> and the <i>Ontological Analysis of Observations and Measurements</i> <a href="#ref4" target="_blank">[4]</a>. The core concepts of environmental data are summarised in <a href="#pic-model">Figure 1. Environmental Data Model</a>. </p>


<p>The central concept of the <i>Observations and Measurements model (O&M) </i><a href="#ref5">[5]</a> is <b>Observation</b>.</p>

<p><b>Observation</b> is a central concept of the model. The goal of the model is to encode the following information about an observation:
<ol>
<li>What the observation applies to? (feature of interest)</li>
<li>What was observed? (observed property)</li>
<li>How the observation was done? (procedure)</li>
<li>What was the result of the observation? (result)</li>
<li>Where the observation happened? (location)</li>
<li>When the observation happened? (time)</li>
</ol>
</p>


<table class="bordered"> <tr>

<td>
<div class="caption"><a name="pic-model">Figure 1. Environmental Data Model</a></div>

<a name="pic-model" href="pics/model.png" target="_blank"><img class="plot" src="pics/model.png" alt="Conceptual model of environmental data." /></a></td>

<td>
<ul>

<li><b>Observation</b> is an act associated with a <u>discrete time instant or period</u> through which a number, term or other symbol is assigned to a <u>phenomenon</u>. It involves application of a specified <u>procedure</u>, such as a sensor, instrument, algorithm or process chain. The procedure may be applied in-situ, remotely, or ex-situ with respect to the sampling location. The <u>result of an observation</u> is an estimate of the value of a <u>property</u> of some feature.</il>

<li><b>Feature of Interest</b> is the target of observation, e.g., an <i>ocean</i> or <i>air</i>.</li>

<li><b>Observed Property</b> is a property of the feature of interest being observed, e.g. <i>CO2 concentration</i> is the property of the air, <i>temperature</i> is a property of the ocean. </li> 

<li><b>Procedure</b> is a procedure used to generate the result of the observation, e.g., a procedure could be an instrument <i>flask</i> that is used to sample air.</li>

<li><b>Result</b> is an estimate of a value of the property of the feature of interest.</li>

<li><b>Location</b> is a geo(-spatial) location of the observation.</li>

<li><b>Time</b> is the observation time, i.e., the time when the result applies to the property of the feature of interest.</li>

</ul></td></tr></table>


<p> <b>Feature</b> is the core notion in all OGC specifications. The <a href="http://portal.opengeospatial.org/files/?artifact_id=41579" target="_blank">OGC specification, p.3</a> defines a feature as an abstraction of real world phenomena. In <a href="#ref4">[4]</a> it is noted that OGC allows two different interpretations of the feature: (1) as a geo-spatial real-world entity (e.g., <i>the Atlantic ocean</i>) or (2) as a pure information object that can represent either a real world entity or a category of real world entities (e.g., <i>ocean</i>). As argued in <a href="#ref4">[4]</a>, it is important to differentiate between these two interpretations, since the possible properties of the feature that can be observed in both cases will be different. For example, the width of a lake can be observed directly or it can be observed on a representation of a lake in a <a href="http://en.wikipedia.org/wiki/Geographic_information_system" target="_blank">GIS</a>. According to the first interpretation, the observed property (depth) is associated with an identifiable object (some lake). Second interpretation suggests that only properties of an information object (e.g., a representation of the lake in GIS) can be observed. To avoid ontological inconsistencies, we follow the suggestion of <a href="#ref4">[4]</a> and <u>consider a feature as an information object that can represent either a real world entity (e.g., <i>the Atlantic ocean</i>) or a category of real world entities (e.g., <i>ocean</i>).</u></p>

<!-- <p>Following the suggestion of <a href="#ref4">[4]</a>, we introduce the notion of <b>Entity of Interest</b> to refer to a real world entity that can play role of a feature of interest (i.e., the target of an observation). This is done to emphasize the ontological distinction between the entity whose properties are observed a feature of interest, since the latter exists only when the observation goes on and stops to exist as soon as the observation is finished, whereas the real world entity remains.</p> -->

<h1> <a name="vocab">II The ENVRI Vocabulary</a> </h1>

<p>The ENVRI vocabulary is the <a href="http://www.w3.org/TR/rdf-schema/">RDF(S)</a> vocabulary that formalises the concept of the Environmental Data Model. The ENVRI vocabulary is available in the <a href="http://data.politicalmashup.nl/RDF/vocabularies/envri" target="_blank">html</a> and <a href="http://data.politicalmashup.nl/RDF/vocabularies/envri.rdf" target="_blank">RDF in RDF/XML</a> or <a href="http://data.politicalmashup.nl/RDF/vocabularies/envri.ttl" target="_blank">RDF in Turtle</a> representations.</p>

<p>The following prefixes are used further on:</p>
<table class="prefixes">
<tr><th>Prefix</th><th>URI</th></tr>

<tr><td>rdf:</td><td><a href="http://www.w3.org/1999/02/22-rdf-syntax-ns#"> &lt;http://www.w3.org/1999/02/22-rdf-syntax-ns#&gt;</a></td></tr>

<tr class="alt"><td>owl:</td><td><a href="http://www.w3.org/2002/07/owl#"> &lt;http://www.w3.org/2002/07/owl#&gt;</a></td></tr>

<tr ><td>dc:</td><td><a href="http://purl.org/dc/elements/1.1/"> &lt;http://purl.org/dc/elements/1.1/&gt;</a></td></tr>

<tr class="alt"><td>xsd:</td><td><a href="http://www.w3.org/2001/XMLSchema#"> &lt;http://www.w3.org/2001/XMLSchema#&gt;</a></td></tr>

<tr ><td>envri:</td><td><a href="http://data.politicalmashup.nl/RDF/vocabularies/envri#"> &lt;http://data.politicalmashup.nl/RDF/vocabularies/envri#&gt;</a></td></tr>

<tr class="alt"><td >gemet-concept:</td ><td ><a href="http://www.eionet.europa.eu/gemet/concept/"> &lt;http://www.eionet.europa.eu/gemet/concept/&gt;</a></td></tr>
<tr ><td >instr:</td><td ><a href="http://sweet.jpl.nasa.gov/2.2/matrInstrument.owl#"> &lt;http://sweet.jpl.nasa.gov/2.2/matrInstrument.owl#&gt;</a></td></tr>
<tr class="alt"><td >units:</td><td ><a href="http://sweet.jpl.nasa.gov/2.2/reprSciUnits.owl#"> &lt;http://sweet.jpl.nasa.gov/2.2/reprSciUnits.owl#&gt;</a></td></tr>
<tr ><td >struc:</td><td ><a href="http://sweet.jpl.nasa.gov/2.2/matrFacility.owl#"> &lt;http://sweet.jpl.nasa.gov/2.2/matrFacility.owl#&gt;</a></td></tr>
<tr class="alt"><td >gsp:</td><td ><a href="http://www.opengis.net/ont/geosparql#"> &lt;http://www.opengis.net/ont/geosparql#&gt;</a></td></tr>

<tr ><td >dbpedia:</td><td ><a href="http://dbpedia.org/resource/"> &lt;http://dbpedia.org/resource/&gt;</a></td></tr>

<tr class="alt"><td >qb:</td><td ><a href="http://purl.org/linked-data/cube#"> &lt;http://purl.org/linked-data/cube#&gt;</a></td></tr>

<!--<tr ><td >envri-vocab:</td><td ><a href="http://data.politicalmashup.nl/RDF/vocabularies/envri#" target="_blank">&lt;http://data.politicalmashup.nl/RDF/vocabularies/envri#&gt;</a></td></tr>

<tr class="alt"><td >envri-dataset:</td><td >&lt;http://example.envri.org/dataset/&gt;</td></tr>
<tr ><td >envri-dsd:</td><td >&lt;http://example.envri.org/structure/dsd/&gt;</td></tr>
<tr class="alt"><td >envri-prop-spec:</td><td >&lt;http://example.envri.org/structure/property-specification/&gt;</td></tr>

<tr ><td >envri-concept:</td><td >&lt;http://example.envri.org/concept/&gt;</td></tr>
<tr class="alt"><td >envri-data:</td><td >&lt;http://example.envri.org/data/&gt;</td></tr> -->

</table><br />

<p>The classes and properties of the ENVRI vocabulary:</p>

<ul>
	<li class="bold">Classes</li>
		<ul>
			<li><code>envri:FeatureOfInterest</code></li>
			<li><code>envri:ObservedProperty</code></li>
			<li><code>envri:Procedure</code></li>
			<li><code>envri:Result</code></li>

		</ul>
	<li class="bold">Properties</li>
		<ul>
			<li><code>envri:hasFeatureOfInterest</code></li>
			<li><code>envri:hasObservedProperty</code></li>
			<li><code>envri:hasProcedure</code></li>
			<li><code>envri:hasResult</code></li>		
			<li><code>envri:hasLocation</code></li>
			<li><code>envri:hasTime</code></li>
		</ul>
</ul>

<p>Note, that the current implementation of the vocabulary contains more terms than presented here. This is because we continued working on the model and vocabulary. The extension to the model is presented in <a href="#extend-model">Section IV Data Model Extension</a>.</p>

<h2> <a name="ontologies">Cross-Domain Ontologies</a> </h2>


<p>We defined the ranges of the properties to be their corresponding classes:</p>

<ul>
	<li><code>envri:hasFeatureOfInterest rdfs:range envri:FeatureOfInterest</code></li>
	<li><code>envri:hasObservedProperty rdfs:range envri:ObservedProperty</code></li>
	<li><code>envri:hasProcedure rdfs:range envri:Procedure</code></li>
	<li><code>envri:hasResult rdfs:range envri:Result</code></li>
</ul>

<p>Two properties, <code>envri:hasLocation</code>, and <code>envri:hasTime</code> were defined using the classes of the existing widely deployed ontologies. </p>

<ul>
	<li><a href="http://www.w3.org/TR/owl-time/" target="_blank">The Time Ontology in OWL</a> is an ontology of temporal concepts.</li>
	<ul>
		<li><code>envri:hasTime rdfs:range time:TemporalEntity</code></li>
	</ul>


	<li><a href="http://www.opengeospatial.org/standards/geosparql" target="_blank">The GeoSPARQL vocabulary</a> is an RDF/OWL vocabulary for representing spatial information consistent with the <a href="http://www.opengeospatial.org/" target="_blank">Open Geospatial Consortium (OGC)</a> Simple Features model.</li>
	<ul>
		<li><code>envri:hasLocation rdfs:range gsp:SpatialObject</code></li>
	</ul>

</ul>

Several modules of <a href="http://sweet.jpl.nasa.gov/" target="_blank">Semantic Web for Earth and Environmental Terminology (SWEET)</a> were re-used, including the <a href="http://sweet.jpl.nasa.gov/2.2/reprSciUnits.owl" target="blank">SWEET Scientific Units</a> module:


<ul>
	<li><code>qb:Attachable units:hasUnit units:Unit</code></li>

</ul>
<!--
Currently, we use classes from two modules of the SWEET ontologies:
		<ul>
			<li><a href="http://sweet.jpl.nasa.gov/2.2/matrFacility.owl" target="_blank">SWEET Material Facility</a></li>
			<li><a href="http://sweet.jpl.nasa.gov/2.2/matrInstrument.owl" target="_blank">SWEET Material Instrument</a></li>
			<li></li>
		</ul>
-->







<h2> <a name="merging">Merging the ENVRI vocabulary with Data Cube</a> </h2>

<p>The ENVRI vocabulary is meant to be an intermediate ontological layer between the specific concepts that describe observations and the RDF Data Cube vocabulary. We plug in the ENVRI vocabulary to Data Cube as follows: we defined the ENVRI properties as sub-properties of one of the <a href="http://purl.org/linked-data/cube#dsd-dimensions" target="_blank">Data Cube component properties</a>.</p>

<ul>
	<li><code>envri:hasFeatureOfInterest rdfs:subPropertyOf qb:AttributeProperty</code></li>
	<li><code>envri:hasObservedProperty rdfs:subPropertyOf qb:MeasureProperty</code></li>
	<li><code>envri:hasProcedure rdfs:subPropertyOf qb:AttributeProperty</code></li>
	<li><code>envri:hasResult rdfs:subPropertyOf qb:DimensionProperty</code></li>
	<li><code>envri:hasLocation rdfs:subPropertyOf qb:DimensionProperty</code></li>
	<li><code>envri:hasTime rdfs:subPropertyOf qb:DimensionProperty</code></li>
</ul>






<h1> <a name="workflow">III Workflow for Environmental Data Harmonisation</a> </h1>

<p>The workflow consists of four main processes illustrated by <a href="#workflow-diagram">Figure 2. Workflow for Environmental Data Harmonisation</a>:</p>
<ul>
	<li><u>Data Acquisition</u>: collection of original data from the research infrastructures.</li>
	<li><u>Data Harmonisation</u>: definition of mappings between the original data, acquired during the previous process, and the target view provided by the <a href="#pic-model">Environmental Data Harmonisation model.</a></li>
	<li><u>Data Conversion</u>: execution of mappings defined during the previous process and materialisation of the harmonised data.</li>
	<li><u>Data Publication</u>: publication and access to the harmonised data.</li>
</ul>

<div class="caption"><a name="workflow-diagram">Figure 2. Workflow for Environmental Data Harmonisation</a></div>
<div class="caption"><a href="pics/workflow.png" target="_blank"><img class="workflow" src="pics/workflow.png" alt="Workflow for Environmental Data Harmonisation" /></a></div>


<h2> <a name="harmonisation">Data Harmonisation Process</a> </h2>



<table class="bordered"> <tr>

<td>
<div class="caption"><a href="#data-harmonisation">Figure 3. Data Harmonisation</a></div>

<a name="data-harmonisation" href="pics/process.png" target="_blank"><img class="plot" src="pics/process.png" alt="Data Harmonisation" /></a></td>

<td>
<ol>
	<li><u>Entity Naming</u></li>
		Entity naming is a process of assigning unique names, HTTP URIs, to the entities of interest found in environmental data and metadata. For this, the obtained data and its metadata are analysed and the entities of interest are identified. The existing Earth Science vocabularies and thesauri are examined for having corresponding terms that can be re-used for the entity naming pro- cess. In case when there was no suitable term found among the terms of the existing resources, a custom HTTP URI is defined.
	<li><u>Semantic Alignment</u></li>
		The named entities from the previous step are further aligned within the ENVRI vocabulary. This is done by defining the named entities as instances of the corresponding ENVRI classes, e.g., envri:FeatureOfInterest. All the classes of the ENVRI vocabulary are listed here http://data.politicalmashup.nl/ RDF/vocabularies/envri.
	<li><u>Schematic Alignment</u></li>
		Alignment at the schema level is done through defining Data Cube structures for environmental data using generic terms of the ENVRI vocabulary. In principle, a Data Cube data structure definition is simply a set of properties that are needed to identify and interpret concrete environmental observations. Such properties might include the feature of interest, i.e., the target of observations; the observed phenomenon, i.e., the property of the target of observations; the procedure used to perform observations, etc.
</ol>

</td></tr></table>











<h1> <a name="extend-model"> IV Data Model Extension </a> </h1>

<h2><a name="sampling-feature">Sampling Feature</a></h2>

<p>In cases when a feature of interest can be analyzed in its entirety, it is directly used to describe the observation. For example, the weight of a specific animal. However, there are cases when observations of properties of features of interest are not made directly on the features. There can be two such cases: (1) a property of a feature of interest is not directly observable, e.g., when the feature is remote; (2) a feature of interest is not accessible, e.g., space. In this case the observation procedure involves samling in representative locations. The observations are then made on a sample of the feature of interest, <b>Sampling Feature</b>, and the feature of interest is called a <b>Sampled Feature</b>. For example, measurements of a concentration of CO2 in the air is done on samples of the air.</p>

<!-- 
When observations are made to estimates properties of a geo-spatial feature, i.e., the feature which property's value varies within a geo-spatial dimension of the feature , sampling features can be classified into:
<ul>
	<li><b>Sampling Point</b>, e.g., station</li>
	<li><b>Sampling Curve</b>, e.g., profile, trajectory</li>
	<li><b>Sampling Surface</b>, e.g., scene, section</li>
	<li><b>Sampling Solid</b>, e.g., mine, lidar cloud</li>
</ul>
 -->

			
<h2><a name="types-of-op">Types of Observed Property</a></h2>

<ul>
<li><b>Simple Properties</b> are single valued properties, e.g., <i>temperature</i>, <i>length</i> and <i>power</i>.</li>
<li><b>Constrained Properties</b> qualify a feature of interest by adding constraints to the observed property. For example, <i>surface-water-temperature</i> constraints the observed property (<i>temperature</i>) on its depth, i.e., <i>surface</i> implies the depth of the water to be 0 to 0.3 meters.</li>
<li><b>Compound Properties</b> properties are:
<ul>
<li><b>Composite Properties</b> are composed of an arbitrary set of properties. These properties may not be related to each other. For example, the observed property <i>weather</i> is composed of a several properties: <i>temperature</i>, <i>rainfall</i>, <i>wind velocity</i>, <i>atmospheric pressure</i>, etc.</li>
<li><b>Series of Properties</b> are composed of homogeneous properties that differ by the value of one or more constraints. For example, spectral channels. <i>Power</i> in 256 channels where <i>wavelength</i> = 0.9-1 &mu;m, 1-1.1&mu;m, 1.1-1.2&mu;m, ...</li>
</ul>
</li>

</ul>

<h2><a name="elaboration-on-procedure">Elaboration on Procedure</a></h2>
<p>Procedure involves <b>Instrument(s)</b> hosted at (a) <b>Facility(ies)</b> on behalf of an <b>Activity</b>.</p>


<ul>
<li><b>Facility</b> is a facility at/from which instruments are hosted or deployed to undertake environmental observation. Three types of facilities exist with respect to the sampling location: 
	<ul>
		<li><b>In-situ</b>: when the sampling feature is co-located with the feature of interest, and the measurement process is performed at this location. An example of the in-situ facilities is <b>Station</b>.</li>
		<li><b>Ex-situ</b>: when the sampling feature is a specimen taken from the feature of interest (e.g., an air sample taken from the air), and the measurement process is performed at a different location.</li>
		<li><b>Remote</b>: when the sampling feature is the feature of interest, and the measurement process is performed at a different location (but no sample taken). An example of the remote sensing activity is <b>Platform</b>.</li>
	</ul>

<li><b>Activity</b> is an activity that generates observations by deploying instruments at one or more facilities. Examples of monitoring activity include: <b>Monitoring Network</b>, <b>Fixed Deployment</b>, <b>Flight</b>, <b>Cruise</b>, <b>Field Trip</b>, <b>Computation</b>, <b>Targeted Programme</b> of <b>Monitoring</b>, etc.</li></ul></ul>


<!-- TT: move this to the main model?? -->
<h2><a name="result-types">Results Types</a></h2>

<p>In principle, observation results may have many datatypes, including primitive types like category or measure, but also more complex types such as time, location and geometry. The type of the result depends on the type of the observed property, e.g., complex results are obtained when the observed property is compound (requires multiple components for its encoding).</p>

<p>When the result of an observation is a numeric value, the observation is a <b>Measurement</b>. Measurements' results must have the <b>Unit of Measure</b> defined. 

<!-- types of the result?
			<li><code>Result</code></li>
				<ul>
					<li><code>envri:PrimaryResult</code></li>
					<li><code>envri:ProcessedResult</code></li>
					<li><code>envri:SimulatedResult</code></li>
				</ul> -->



<h2><a name="vocab-extension">Extension of the ENVRI vocabulary</a></h2>

<p>As the result of the model extension, we implemented the extension of the ENVRI vocabulary</p>

<ul>
	<li class="bold">Classes</li>
		<ul>
			<li><code>envri:SamplingFeature</code></li>			
			<li><code>envri:Activity</code></li>
			<li><code>struc:Facility</code></li>
				<ul>
					<li><code>envri:InSituFacility</code></li>
					<li><code>envri:ExSituFacility</code></li>
					<li><code>envri:RemoteFacility</code></li>
				</ul>
		</ul>
	<li class="bold">Properties</li>
		<ul>
			<li><code>envri:involves</code></li>
			<li><code>envri:hostedAt</code></li>
			<li><code>envri:initiatedBy</code></li>
			
		</ul>
</ul>

<p><code>envri:SamplingFeature</code> is defined as a sub-class of <code>owl:Class</code>. The relationships between the classes and properties are captured by the <a href="#pic-model-extension">Figure 3. Environmental Data Model Extension</a>. The ENVRI terms are in blue.</p>

<div class="caption"><a name="pic-model-extension">Figure 3. Environmental Data Model Extension</a></div>
<div class="caption"><a name="pic-model-extension" href="pics/model-extension.png" target="_blank"><img class="plot" src="pics/model-extension.png" alt="Extension of the conceptual model of environmental data." /></a></div>



<h1> <a name="resources">V Resources</a> </h1>

<ul>
	<li>The ENVRI internal report on <a href="documentation/envri-report.pdf" target="_blank">Task 4.2: Linked Data Component for Environmental Data Harmonization</a>

<li>The "Iceland Volcano Ash" study case demonstrating the usage of the Linked Data Harmonisation Model for Environmental Data is described <a href="case-study.html" target="_blank">here</a>.</li>
<li>The paper <a href="http://link.springer.com/chapter/10.1007%2F978-3-642-41360-5_25#page-1" target="_blank">Semantically-Enabled Environmental Data Discovery and Integration: demonstration using the Iceland Volcano Use Case</a> was presented during the conference on <a href="http://kesw.ru/" target="_blank">Knowledge Engineering and Semantic Web, in October 2013</a>. The pre-printed version of the paper is available <a href="documentation/KESW2013-ENVRI-system-demo.pdf" target="_blank">here</a></li>
<!--<li>The definition of the study case was done during the internal project workshop in Helsinki in November 2012. </li>-->
</ul>


</ul>
<br />




<h1> <a name="references">VI References</a> </h1>

<ol>
<a name="ref1"><li>Rüther, M., Fock, J., and Hubener, J.: <a href="http://www.w3.org/egov/wiki/images/4/46/LinkedEnvironmentData-EI2010final.pdf" target="_blank">Linked Environmental Data</a>. 24th International Conference on Informatics for Environmental Protection (2010)</li></a>

<a name="ref2"><li>Shaon, A., Woolf, A., Boczek, R., Rogers, W., and Jackson, M.: <a href="http://ceur-ws.org/Vol-798/paper6.pdf" target="_blank">An Open Source Linked Data Framework for Publishing Environmental Data under the UK Location
Strategy</a>. Proceedings of the Terra Cognita Workshop on Foundations, Technologies and Applications of the Geospatial Web (2011)</li></a>


<a name="ref3"><li>Rüther, M., Fock, J., and Hubener, J.: <a href="http://www.semantic-web-journal.net/system/files/swj457.pdf" target="_blank">The ACORN-SAT Linked Climate Dataset</a>. Semantic Web Journal (2013)</li></a>

<a name="ref4"><li>Probst, F.: <a href="http://ifgi.uni-muenster.de/~probsfl/publications/PROBST%20Ontological%20Analysis%20of%20Observations%20and%20Mesurements.pdf" target="_blank">Ontological Analysis of Observations and Measurements</a>. 4th International Conference on Geographic Information Science (2006)</li></a>

<a name="ref5"><li><a href="http://portal.opengeospatial.org/files/?artifact_id=41579" target="_blank">The standard conceptual model of observations and measurements</a> endorsed by <a href="http://www.opengeospatial.org/" target="_blank">OGC</a> and <a href="http://www.iso.org/iso/home.html" target="_blank">ISO</a></li></a>

</ol>
