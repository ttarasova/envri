\section{The ENVRI Vocabulary: Structure}\label{sec:vocabStructure}

The ENVRI vocabulary, the vocabulary to represent ENVRI data. \todo{MM}{turn into a sentence}
The first version of the vocabulary is published at \cite{envri-vocab}. The vocabulary consists of two types of concepts that were identified during the analysis of environmental data in Section~\ref{sec:model}: structural and data concepts. In this section we introduce the RDF Data Cube vocabulary that will be re-used to encode the structural concepts of environmental data in RDF. Section~\ref{sec:vocabData} discusses the process of population of the structure of the Environmental Data Vocabulary with the data concepts.

\paragraph{}
The following prefixes are used throughout this section and further in this report:\\

\begin{tabular}{|l|l|}
\hline
$\textbf{prefix}$ & $\textbf{namespace URI}$ \\
\hline
$rdf$ & \url{http://www.w3.org/1999/02/22-rdf-syntax-ns\#} \\
\hline
$rdfs$ & \url{http://www.w3.org/2000/01/rdf-schema\#} \\
\hline
$owl$ & \url{http://www.w3.org/2002/07/owl\#} \\
\hline
$qb$ & \url{http://purl.org/linked-data/cube\#} \\
\hline
$time$ & \url{http://www.w3.org/2006/time\#} \\	
\hline
$gsp$ & \url{http://www.opengis.net/ont/geosparql\#} \\
\hline
\end{tabular}

\paragraph{}
In addition to the prefixes above, we will use the $ex$ prefix to refer to the URI of the ENVRI vocabulary. For the demo purposes we use the following authority $http://example.envri.org/$ part of the URI of the ENVRI vocabulary and design URIs for different types of concepts within this example authority. For example, the structures of data provided by $\{provider\}$ will be served by the following URI: $\textbf{http://example.envri.org/structure}/\{\emph{provider}\}$. For the sake of clarity, throughout this work we will use $ex$ as a prefix to refer to any term from the the ENVRI vocabulary. All the patterns of URIs of different concepts are presented at the supplementary web page \cite{supply-uris}. 

\paragraph{The RDF Data Cube vocabulary}
The RDF Data Cube vocabulary \cite{datacube} is commonly used as a generic framework to publish multi-dimensional data in RDF. The Data Cube model was built on the cube model that underlies the Statistical Data and Metadata eXchange \cite{sdmx} (SDMX), an ISO standard for exchanging and sharing statistical data and metadata. The Data vocabulary is currently in the processed of becoming a W3C Recommendation. 

Figure~\ref{fig:datacube} illustrates the core data model of the RDF Data Cube vocabulary. 

\begin{figure}[h]
  \caption{Core model of the Data Cube vocabulary}\label{fig:datacube}
  \centering
    \includegraphics[width=0.9\textwidth]{datacube}
\end{figure}

\texttt{qb:Observation} belongs to \texttt{qb:DataSet} which has a particular structure defined by \texttt{qb:DataStructureDefinition}. The structure is composed by a set of \texttt{qb:ComponentProperty} specified by \texttt{qb:ComponentSpecification}.

 
\subsection{Observation and Data Set}
The concept of \textit{Observation} in Data Cube corresponds to the concept of observation in the conceptual model of environmental data. The concept of \textit{DataSet} in Data Cube is defined as follows:

\begin{description}
	\item[Data Set] is a collection of observations that are grouped according to some criteria.
\end{description}

We re-use this definition for the ENVRI vocabulary. As an example of an environmental data set consider \textit{observations of the concentration of carbon dioxide in the air in 2010}. The grouping criterion for these observations is the year when they were made.

\subsection{Component Properties}
\begin{sloppypar}
Data Cube introduces three types of the component properties:
\begin{itemize}
	\item \texttt{qb:MeasureProperty} defines what was observed, e.g., \texttt{ex:observedFeatureOfInterest};
	\item \texttt{qb:AttributeProperty} allows us to qualify and interpret the observed value(s), e.g., \texttt{ex:observedPropertyOfFeature}, \texttt{ex:hasUnitOfMeasurement} and \texttt{ex:usedProcedure};
	\item \texttt{qb:DimensionProperty} contextualizes observations with a data set, e.g., \texttt{ex:observedAtLocation} and \texttt{ex:observedAtTime}.
\end{itemize}

The values of the component properties are given by instances of the corresponding classes:
\begin{itemize}
	\item \texttt{ex:observedFeatureOfInterest} has range \texttt{ex:FeatureOfInterest}
	\item \texttt{ex:observedPropertyOfFeature} has range \texttt{ex:PropertyOfFeature}
	\item \texttt{ex:hasUnitOfMeasurement} has range \texttt{ex:UnitOfMeasurement}
	\item \texttt{ex:usedProcedure} has range \texttt{ex:Procedure}
	\item \texttt{ex:observedAtTime} has range \texttt{time:TemporalEntity}
	\item \texttt{ex:observedAtLocation} has range \texttt{gsp:SpatialObject}
\end{itemize}
\end{sloppypar}

Note, that we re-used two classes, \texttt{time:TemporalEntity} and \texttt{gsp:SpatialObject}, from  existing vocabularies:
\begin{itemize}
	\item{} The range of the \texttt{ex:observedAtLocation} is defined by \texttt{gsp:SpatialObject}, the class that is provided by the GeoSPARQL Ontology \cite{geosparql}. GeoSPARQL is defined by the Open Geospatial Consortium \cite{OGC} (OGC) as a core vocabulary for representing geospatial information in RDF.
	\item{} The range of \texttt{ex:observedAtTime} is defined by the class \texttt{time:TemporalEntity} of the Time Ontology in OWL \cite{time}. This ontology is developed by the Ontology Engineering and Patterns \cite{OEP} Task Force (OEP). The ontology provides an expressive vocabulary that can be used to represent both time intervals and time instants with different level of granularity up to seconds.
\end{itemize} 

The other classes are defined in the ENVRI vocabulary. 

\subsection{Component Specifications}
Component specifications are used in Data Cube to specify the component properties. In the simplest case a component specification just references the corresponding component property. Figure~\ref{fig:cs1} illustrates an example of the simplest encoding of a component specification.

\begin{figure}[h]
  \caption{Simplest component specification}\label{fig:cs1}
  \centering
    \includegraphics[width=0.7\textwidth]{cs1}
\end{figure}

By default, each component property is attached to \texttt{qb:Observation}. This means that each observation will have its own value of this component property. Indeed, each observation will have its own \texttt{time:TemporalEntity} indicating the time when the observation was made. However, there are component properties which values will be shared by all the observations of a given data set. Component specification allows us to define whether the value of the component property should be attached to a data set or to each observation. This is done through the  \texttt{qb:componentAttachment} property. Figure~\ref{fig:cs2} illustrates the usage of this property to define that unit of measurement is the same for all the observations of a given data set.

\begin{figure}[h]
  \caption{Specification of a component property common to a data set}\label{fig:cs2}
  \centering
    \includegraphics[width=0.7\textwidth]{cs2}
\end{figure}




\subsection{Structure Definition}\label{sec:dsd}
The component specifications are combined into a \texttt{qb:DataStructureDefinition}. Figure~\ref{fig:dsd} represents an example of encoding of a structure of an environmental data set. The example structure consists of the component properties that have been identified so far\footnote{For the sake of clarity of the given diagram, we didn't define types for all the component specifications}.

\begin{figure}[h]
  \caption{Example structure of an environmental data set}\label{fig:dsd}
  \centering
    \includegraphics[width=0.9\textwidth]{dsd}
\end{figure}


