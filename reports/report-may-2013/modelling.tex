\section{Environmental Data Modelling}\label{sec:model}

In this section an analysis of the environmental data is presented in Section~\ref{sec:concepts}. Common concepts are identified and their definitions are provided. The conceptual model of the environmental data is provided and the RDF Data Cube vocabulary is discussed as a framework to encode the structural elements of the conceptual model of the environmental data in RDF.

\subsection{Environmental Data Analysis}\label{sec:concepts}

Environmental data is diverse in nature. However, all environmental data share similar fundamental concepts. The analysis of the common concepts of environmental data was based on the OGC conceptual model for handling observations \cite{OGC-OM} and the ontological analysis of the OGC model given in \cite{OGC-OM-analysis}. \todo{MM}{tell us what OGC stands for}

The central concept of the O\&M model is observation. \todo{MM}{what is that? the O \& M model? Is that OGC?}

\begin{description}
	\item[Observation] is an event that estimates the value of environmental phenomenon using a specified procedure.
\end{description}

For example, consider an observation of the \textit{carbon dioxide concentration in the air}. \textit{Carbon dioxide concentration in the air} is the observed phenomenon in the example observation. The observed phenomenon is a composite concept. It consists of the actual \underline{observed property} (\textit{concentration in the air}) of some \underline{feature of interest} in the environment (\textit{carbon dioxide}).

\begin{description}
	\item[Feature of interest] is the target of an observation.
	\item[Property of feature] is the property of the feature of interest of an observation.
\end{description}

In order to make observations, specified \underline{procedures} are established. The procedure can be the platform, instrument or sensor used for an observation. It can also be a process chain, human observer, a computation or simulator.

\begin{description}
	\item[Procedure] is the tool(s) to perform an observation.
\end{description}

The result of the procedure is an estimate \underline{value} (a number, term or other symbol) assigned to an observation. For example, the result of the observation of the concentration of carbon dioxide in the air is \textit{391.318 ppm}''. For the correct interpretation of the result, the \underline{unit of measurement} is provided (\textit{ppm - parts per million}).

\begin{description}
	\item[Value] is the result of an observation.
	\item[Unit of Measurement] is a quantity used to measure the observed phenomenon.
\end{description}

An observation is associated with a particular \underline{time} and \underline{location}. For example, the result of the example observation was obtained \textit{on January 1st, 2010 at 00:00} and was made at \textit{(53$\circ$ 19'N, -9$\circ$ 54'W) 25m above the sea level}.

\begin{description}
	\item[Time] is the time when an observation was made.
	\item[Location] is the location of an observation.
\end{description}

\subsection{Conceptual Model}\label{sec:conceptualModel}

Figure~\ref{fig:model} illustrates the conceptual model of environmental data that consists of the common concepts that were identified in Section~\ref{sec:concepts} and relationships between them.

\begin{figure}[h]
  \caption{Conceptual Model of Environmental Data}\label{fig:model}
  \centering
    \includegraphics[width=0.8\textwidth]{model}
\end{figure}

The star-like model of the environmental data is centered around the main \underline{structural concept} of \textit{Observation}. Other concepts are used to provide the context to the observation: what was observed, when and where; what procedure was used to perform the observation; what is the value of the observation and what is the unit of measurement in which the value is given. These concepts can be generalized to one structural concept \textit{Property}.

During the analysis of environmental data we identified seven different instances of \textit{Property}: \textit{Feature of Interest}, \textit{Property of Feature}, \textit{Procedure}, \textit{Value}, \textit{Unit of Measurement}, \textit{Time} and \textit{Location}. Different data may require different properties (see Section~\ref{sec:vocabData}). In contrast with the structural concepts, we will call the instances of \textit{Property} \underline{data concepts}.

