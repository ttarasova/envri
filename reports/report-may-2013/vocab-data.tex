\section{The ENVRI Vocabulary: Data}\label{sec:vocabData}

In Section~\ref{sec:vocabStructure} we discussed the structural components of the ENVRI vocabulary that are represented by the RDF Data Cube vocabulary. Three core structural components of the vocabulary were identified:

\begin{itemize}
	\item{} \texttt{qb:Observation}
	\item{} \texttt{qb:DataSet}
	\item{} \texttt{qb:ComponentProperty}
\end{itemize}

\begin{sloppypar}
The instances of \texttt{qb:Observation} and \texttt{qb:DataSet} are specific for concrete observation and data set, whereas instances of \texttt{qb:ComponentProperty} can be re-used across different observations and data sets. For example, consider the component property \texttt{ex:observedFeatureOfInterest}. Every observation must have this property attached.

Another re-usable concept is the range of \texttt{qb:ComponentProperty}. For example, \texttt{CO2} is a particular feature of interest observed by some observation. There can be another observation that observed the same feature of interest, but, for instance, at a different time. 

We will call these re-usable concepts \underline{unifying}, because with their help we will build a common unifying vocabulary that could be used to encode different data.

In the rest of section we will analyze concrete environmental data. We will demonstrate how to populate the ENVRI vocabulary with the unifying concepts coming from different ENVRI Research Infrastructures. 
\end{sloppypar}

\subsection{The ``Iceland Volcano Ash'' Case Study}\label{sec:caseStudy}

The ``Iceland Volcano Ash'' case study was defined in \cite{workshopDoc} as follows:
\begin{description}
\item[] \textit{``be able to at least discover the specific event in various data sets, and see if correlations are relevant (Volcano outburst April 10, 2010 and onwards'')}.
\end{description}

For the study case, the following Research Infrastructures will provide data:
\begin{itemize}
	\item ICOS
	\item ACTRIS
	\item EISCAT-3D
	\item Euro-Argo
\end{itemize} 

We will introduce a new component property \texttt{ex:isProvidedBy}. This property will be used to link environmental data to its provider. The range of this property will be the values of the class \texttt{ex:DataProvider}. At the moment we populate the ENVRI vocabulary with four instances of \texttt{ex:DataProvider}:
\begin{itemize}
	\item \texttt{ex:ACTRIS}
	\item \texttt{ex:ICOS}
	\item \texttt{ex:EISCAT-3D}
	\item \texttt{ex:Euro-Argo}
\end{itemize} 

To facilitate the search of data based on the discipline where it comes from, we will introduce an additional component property: \texttt{ex:isFromEnvironmentalDiscipline}. The range of the property is \texttt{ex:EnvironmentalDiscipline} class. The following scientific disciplines were identified:

\begin{itemize}
	\item \texttt{ex:Atmospheric-science}
	\item \texttt{ex:Oceanography}
	\item \texttt{ex:Geo-space-monitoring}
	\item \texttt{ex:Climatology}
\end{itemize}

The complete list of the component properties is as follows:
\begin{itemize}
	\item \texttt{ex:observedFeatureOfInterest}
	\item \texttt{ex:observedPropertyOfFeature}
	\item \texttt{ex:hasUnitOfMeasurement}
	\item \texttt{ex:usedProcedure}
	\item \texttt{ex:observedAtTime}
	\item \texttt{ex:observedAtLocation}
	\item \texttt{ex:isProvidedBy} 
	\item \texttt{ex:isFromEnvironmentalDiscipline}
\end{itemize}

\subsection{ICOS}
Integrated Carbon Observation System (ICOS) is a distributed infrastructure for monitoring greenhouses through its atmospheric, ecosystem and ocean networks. For the case study we will consider data from the atmospheric network\footnote{\url{https://icos-atc-demo.lsce.ipsl.fr/homepage}} (ICOS ATC). 

Before we proceed with the data structure definition of the ICOS data, we will analyze the data and extend the environmental vocabulary with the additional properties relevant for the ICOS data.

\paragraph{Station}
ICOS ATC network consists of several atmospheric stations in Europe. To trace the source of the data, we will introduce a new component property: \texttt{ex:observedAtStation}, which will have the range the \texttt{ex:Station} class. For the case study we will consider the following stations:
\begin{itemize}
	\item \texttt{ex:TRN} - TRAINOU (France)
	\item \texttt{ex:PUY} - PUY DE DOME (France)
	\item \texttt{ex:MHD} - MACE HEAD (Ireland)
	\item \texttt{ex:SMEARII} - SMEAR II (Finland)
\end{itemize}

In the conceptual model of environmental data (see Section~\ref{sec:model}), procedure was defined as tool(s) that were used to perform observations. The observations of CO2 concentration provided by ICOS were made by \textit{flasks}. We will introduce a new component property to define the ICOS procedure: \texttt{ex:usedInstrument}. {ex:Flask} is the value of the property is the instance of the custom class {ex:Instrument}.

\begin{itemize}
	\item \texttt{ex:Flask} - glass container to sample air
\end{itemize}


\paragraph{Data Type}
\begin{sloppypar}
ICOS ATC data have four processing levels. We will use \texttt{ex:hasProcessingLevel} to specify this information. We define the class \texttt{ex:ProcessingLevel} with the following instances:
\end{sloppypar}
\begin{itemize}
		\item \texttt{ex:Level0} - raw data, e.g., current or voltages, depending on the instrument
		\item \texttt{ex:Level1} - parameters expressed in geophysical units, e.g., ppm-CO2; this level is divided into:
			\begin{itemize}
				\item \texttt{Level1a} - near real-time data
				\item \texttt{Level1b} - long term validated data
			\end{itemize}
		\item \texttt{Level2} - elaborated products, e.g., gap-filling or selection
		\item \texttt{Level3} - added value products, e.g., plots
\end{itemize}

\subsubsection*{ICOS data structure definition}
Figure~\ref{fig:icos-dsd} presents the data structure definition of the ICOS data.
\begin{figure}[h]
  \caption{The data structure definition of the ICOS data}\label{fig:icos-dsd}
  \centering
    \includegraphics[width=1\textwidth]{icos-dsd}
\end{figure}

In order to encode ICOS ATC observations, we need to use the component properties defined by the structure. All the components, except for \texttt{ex:observedAtTime} and \texttt{ex:observedAtLocation}, were defined as common to the whole data set. \todo{MM}{Can you explain why? They seem on first sight common to all datsets.}


\subsubsection*{ICOS data sets}
Let's consider an example ICOS data set. Fig. ~\ref{fig:icos-dataset} represents a fragment of the ICOS data set that contains measurements of the concentration of CO2 made by the Mace Head station in 1993. 

\begin{figure}[h]
  \caption{Fragment of the ICOS data set}\label{fig:icos-dataset}
  \centering
    \includegraphics[width=0.6\textwidth]{icos-dataset-fragment}
\end{figure}

Figure~\ref{fig:icos-dataset-example} illustrates the encoding of the example data set.
\begin{figure}[h]
  \caption{ICOS data set}\label{fig:icos-dataset-example}
  \centering
    \includegraphics[width=0.9\textwidth]{icos-dataset-example}
\end{figure}

Note, that the structure of the data set is given by the Data Cube property \texttt{qb:structure}.

\subsection{ICOS observations}
Note, that the values of \texttt{ex:observedAtTime} and \texttt{ex:observedAtLocation} were not specified. This is due to the fact that these values are specific to each observation. Figure~\ref{fig:icos-observation} demonstrates encoding of an observation of the example data set (taken from the row number 5).

\begin{figure}[h]
  \caption{ICOS observation}\label{fig:icos-observation}
  \centering
    \includegraphics[width=0.9\textwidth]{icos-observation}
\end{figure}

Note, that the value of the observation is provided by the property \texttt{rdf:value}, instead of the introduced before \texttt{ex:hasResult}. The observation is connected to the data set containing the observation through the Data Cube property \texttt{qb:dataSet}. All the values of the properties that were defined for the data set are also valid for all the observations of this data set. For example, we know that the observation illustrated by fig.~\ref{fig:icos-observation} provides \texttt{ex:CO2} \texttt{ex:Concentration-in-air} that was measured by \texttt{ex:Flask}.