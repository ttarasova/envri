\section{Introduction}\label{sec:intro}

\subsection{Needs and Challenges of Environmental Research}\label{sec:env-research}

Environmental research is an interdisciplinary research. The laboratory of environmental scientists is the whole Earth. The Earths' systems are characterized by a multitude of interrelations between different natural processes, such as Earth deformation processes and plate tectonics, physical and chemical dynamics of the atmosphere, marines systems and the living world. In order to study a phenomenon of the environment of our Planet, it is not enough to look at the Earth from one discipline. Investigation of correlations between data observed in different Earth' systems is required. 

Let's consider a study case of the eruption of the Eyjafjallajok\"{u}ll volcano in Iceland in March 2010\footnote{The study case was defined during the ENVRI project's workshop in Helsinki. More information about the study case can be found at \url{http://envri.eu/group/envri/documents/-/document_library/view/813457}.}
\vspace{3 mm}
\begin{addmargin}[2em]{2em}
	\textit{``be able to at least discover the specific event in various data sets, and see if correlations are relevant Volcano outburst April 10, 2010 and onwards)''}
\end{addmargin}
\vspace{3 mm}

Assuming that the researcher wants to investigate correlations of the concentration of CO2 and the ocean temperature in the area of the volcano eruption. The former can be found in a research infrastructure that deals with atmospheric measurements, whereas, the latter is provided by an ocean monitoring infrastructure. Different research infrastructures use different systems, technologies and standards to represent, describe, store, manage and publish their data. This makes the data inherently heterogeneous at many levels \cite{interoper}:


\begin{itemize}
	\item[-] \textbf{\textit{system and technological data heterogeneity}}, that includes both information system heterogeneity (e.g., different data management systems) and the platform heterogeneity (e.g., different operating systems);
	\item[-] \textbf{\textit{structural data heterogeneity}}, that stems from different data models used in different Earth Science disciplines;
	\item[-] \textbf{\textit{schematic data heterogeneity}} appears when data sources use the same data model, but comply with different schemes;
	\item[-] \textbf{\textit{syntactic data heterogeneity}}, i.e., the use of different formats to represent data (e.g., CSV, relational data, NetCDF, spreadsheets, etc.);
	\item[-] \textbf{\textit{semantic data heterogeneity}} is the difference in terminology and semantic interpretation of the data.
\end{itemize}


While the existing data integration solutions are able to provide interoperability at system, syntactic, and structural levels \cite{interoper,3decades}, the key challenges to be faced are at the semantic level. Which becomes even harder in the interdisciplinary setting of environmental research, where scientists work with data that spans multiple disciplines, including the disciplines in which they are not very well trained. Indeed, a meteorologist is unlikely to understand the terms used to describe data in oceanography.


\subsection{Task 4.2 Objectives}\label{sec:objectives}

Within the task 4.2 \textit{Data Integration, Harmonization and Publication Facilities}, we worked on a solution for ``integration and harmonisation of data resources from cluster's infrastructures and publication according unifying views'' \cite{project-description}. The requirements to a potential solution as they are stated in the project's description can be summarised as follows:

\begin{description}	
	\item[R1] ``The developed components are made available in such a way that ENVRI cluster infrastructures can use them in their own context for addressing their specific needs. Whenever possible solutions developed in the context of existing (e-)infrastructure projects and existing resources are expected to be reused.''
	
	\begin{itemize}
		\item[$\rightarrow$] \textit{Thus, we need a solution that builds on top of the existing research infrastructures and not replaces them.}	
	\end{itemize}
		
	\item[R2] ``In particular, the developed components rely on data resources, like code lists, thesauri, and ontologies, developed by the different communities of practice to serve their interoperability needs.''
	
	\begin{itemize}
		\item[$\rightarrow$] \textit{As the analysis of the research infrastructures showed \cite{d31}, some research communities have standards ways to represent data and metadata (e.g., NetCDF in the oceanography), while others do not have yet established standards. Thus, the potential solution for data integration should also be flexible for changes in the existing resources for data description. For example, it should be possible to add a new vocabulary when it emerges.}	
	\end{itemize}		
	
\end{description}


\subsection{Linked Data Approach}\label{sec:ld}

The analysis of the related work presented in Section~\ref{sec:relwork} showed that the Linked Data publishing technology has a great potential for integrating heterogeneous data. The Linked Data principles have been successfully applied in different domains, including open government data\footnote{\url{http://data.gov.uk/} Last-accessed: March 2014}, statistics\footnote{\url{http://270a.info/} Last-accessed: March 2014} and bioinformatics \footnote{\url{https://github.com/bio2rdf} Last-accessed: March 2014}. In our work we study the benefits and opportunities of Linked Data for environmental research.


\paragraph{Linked Data}
Linked Data refers to a set of best practices for publishing and connecting structured data in the Web using the existing Web standards: HTTP \cite{HTTP} URI \cite{URI}, RDF \cite{RDF} and SPARQL \cite{SPARQL}. The principles for publishing Linked Data are the following \cite{ld}:

\begin{itemize}
	\item Use URIs as names for things
	\item Use HTTP URIs so that people can look up those names
	\item When someone looks up a URI, provide useful information, using the standards (RDF, SPARQL)
	\item Include links to other URIs, so that more things can be discovered
\end{itemize}

What makes Linked Data different from the traditional Web publishing technologies is the ability to publish and link on the Web not only documents, but also entities that compose these documents. These can be real-world entities, like \textit{the city of Amsterdam}, or abstract concepts, like \textit{weather} or \textit{environmental observation}. Linked Data allows to identify, describe and interconnect not only collections of observations, but also individual observations that constitute these collections. For example, with Linked Data it is possible to refer to a single cell in a spreadsheet with daily observations of average temperature in Amsterdam in 2013\footnote{A cell would correspond to the average temperature in Amsterdam in a specific day in 2013.}. 


The following is the main research idea that motivated the usage of Linked Data for environmental data publication, harmonisation and integration:

\vspace{3 mm}

\begin{addmargin}[2em]{2em}
	\textit{Linked Data allows for data representation at much finer granularity than a document level. Such granular representation brings new opportunities for working with environmental data, e.g., annotating single observations or groups of observations and querying and integrating them from different repositories in a more flexible way.}
\end{addmargin}


\subsubsection*{Linked Data Integration Approach}\label{sec:ld-approach}

Linked Data was proposed as a novel solution for data integration at a Web scale \cite{ld-book, ld-bizer}. Next, we outline the approach of Linked Data to resolving different types of data heterogeneities introduced in Section~\ref{sec:intro}.

\paragraph{System/Technological Data Heterogeneity}

Linked Data functions as a technological bridge between different data providers. It does not replace the existing technologies established in different infrastructures, but rather complements them with an additional representational layer. This meets the project's requirement \textbf{R1}.


\paragraph{Structural/Syntactic Data Heterogeneity}

Linked Data provides a single model to represent data, RDF. There are different formats to serialise RDF (e.g., RDF/XML or N-Triples), which can be easily converted to one another. 

\paragraph{Schematic Data Heterogeneity}

RDF is a common model to publish Linked Data, but different schema can be used to model different data. Schema is ``understood in the Linked Data context as the mixture of distinct terms from different RDF vocabularies that are used by a data source to publish data on the Web. This mixture may include terms from widely used vocabularies ... as well as proprietary terms'' \cite{ld-book}. Linked Data follows two approaches to deal with heterogeneous vocabularies:

\begin{enumerate}
	\item \underline{adopt existing and widely deployed vocabularies}, so that the data becomes interoperable with other Linked Data sets that were described using the same vocabularies;
	\item if the existing vocabularies are not enough to publish Linked Data, one can \underline{define a custom vocabulary}; the terms of such vocabularies should be described and linked to the existing vocabularies.
\end{enumerate}


\paragraph{Semantic Data Heterogeneity}

Linked Data relies on ontologies to resolve semantic data heterogeneity. Moreover, multiple ontologies are allowed to describe the same concepts, when it is not possible to agree on a common terminology. This supports the idea of having different perspectives on the same domain. We find this to be a natural fit to the complex interdisciplinary settings of the environmental science, where there is no single metadata standard used in all the disciplines by all the data providers.



\subsection{Content}

In this document we dedicate a section to survey of the works related to environmental data integration in Section~\ref{sec:relwork}. We used this study to guide development of a harmonisation model for environmental data, which is explained in Section~\ref{sec:model}. After giving the intuition to the main concepts of environmental data, we introduce the conceptual model for environmental data and explain its implementation which resulted in the ENVRI vocabulary. In Section~\ref{sec:workflow} we propose a workflow for harmonising and integrating heterogeneous environmental data using the ENVRI vocabulary. Section~\ref{sec:case-study} contains implementation of the workflow demonstrated on one of the study cases of ENVRI, ``Iceland Volcano Ash''. In Section~\ref{sec:conclusion} we share our findings on pros and cons of the presented Linked Data based approach for environemntal data harmonisation. Future directions conclude the report.



 

