\section{Iceland Volcano Ash Case Study}\label{sec:case-study}

ENVRI introduced study cases to demonstrate usage of the developed solutions for data integration. We used the ``Iceland Volcano Ash'' case study which was defined during the internal project's workshop \cite{workshopDoc} as follows:

\vspace{3 mm}
\begin{addmargin}[2em]{2em}
	\textit{``be able to at least discover the specific event in various data sets, and see if correlations are relevant Volcano outburst April 10, 2010 and onwards)''}
\end{addmargin}
\vspace{3 mm}


For the study case, we used the data from following research infrastructures:
\begin{itemize}
	\item The Atmospheric Thematic Center of the Integrated Carbon Observation System (ICOS ATC)\footnote{\url{https://icos-atc-demo.lsce.ipsl.fr/} Last-accessed: March 2014}
	\item The European contribution to Argo program (Euro-Argo)\footnote{\url{http://www.euro-argo.eu/} Last-accessed: March 2014}
\end{itemize} 

In the rest of the section we explain implementation of the Linked Data Harmonisation workflow presented in Section~\ref{sec:workflow} on the ``Iceland Volcano Ash'' case study, The study case is also published at \url{http://staff.science.uva.nl/~ttaraso1/html/case-study.html}. 

\subsection{Data Acquisition}\label{sec:data}

In our realisation of the study case we obtained data from the research infrastructures and stored it locally. Alternatively, the source data could be requested from its original repositories at a run time. This alternative approach would require us to have online access to the data, which was not the case with the ICOS data from the Puy De Dome atmospheric station. The data from 2010 was not published at the ICOS ATC website. 

The acquisition of data was done in a semi-automatic mode. Collections of the ICOS atmospheric measurements of CO2 from the Mace Head and Puy De Dome stations were received by email. The rest of the ICOS data (near real time data) was scraped from the ICOS ATC website using the dedicated \texttt{wget} requests. As the result, 30 CSV files were obtained (in total 14.4MB).

The Euro-Argo data was collected using the Euro-Argo data selection service\footnote{\url{http://www.argodatamgt.org/Access-to-data/Argo-data-selection} Last-accessed: March 2014}. We obtained observations from the four Euro-Argo platforms with platform ID: 4901091, 4900503, 4900679 and 4901101, in four CSV files of 2MB in total.

All the obtained data can be downloaded from \url{https://bitbucket.org/ttarasova/envri/} under the \texttt{study-case/data} folder. More characteristics of the obtained data are given at \url{http://staff.science.uva.nl/~ttaraso1/html/case-study.html#acquisition}.

\subsection{Data Harmonisation}\label{sec:data-harmonisatio}

Refer to \url{http://staff.science.uva.nl/~ttaraso1/html/case-study.html#harmonisation} for the detailed discussion of the process of data harmonisation. In this report we will consider an example of the ICOS observations highlighted in Fig.~\ref{fig:icos-dataset-fragment}:

\begin{figure}[h]
  \caption{Fragment of the ICOS data set}\label{fig:icos-dataset-fragment}
  \centering
    \includegraphics[width=0.5\textwidth]{pics/icos-dataset-fragment}
\end{figure}

Using the metadata information about the Mace Head station that provided the example observation\footnote{\url{https://icos-atc-demo.lsce.ipsl.fr/mace-head-observatory} Last-accessed: March 2014} and the metadata of the ICOS observations\footnote{\url{https://icos-atc-demo.lsce.ipsl.fr/dataset} Last-accessed: March 2014}, we interpret the highlighted observation as follows:

\vspace{3 mm}
\begin{addmargin}[2em]{2em}
	\textit{391.318 parts per million is the result of the observation of the concentration of CO2 in the air on 2010-01-01 at 00:00 made at (53$\circ$ 19'N, -9$\circ$ 54'W) at 25 meters above sea level using the UHEI flask sampler}
\end{addmargin}
\vspace{3 mm}


\subsubsection*{Entity Naming}

The goal of this step is to assign HTTP URIs to the entities of interest. Following the Linked Data principles, we search among the existing resources that we could re-use for the entity naming step.


\paragraph{Existing Earth Science Vocabularies}
Due to the interest to Linked Data from various national and international organisations that work on metadata standardisation, there is a number of terminological resources available in RDF, e.g., the GEneral Multilingual Environmental Thesaurus (GEMET)\footnote{\url{http://www.eionet.europa.eu/gemet/rdf} Last-accessed: March 2014} and the German cross-thematic environmental thesaurus UMTHES\footnote{\url{http://data.uba.de/umt/de.html} Last-accessed: March 2014}. A number of thesauri is published by the Joint Research Centre of the European Commission in SKOS using RDF/XML\footnote{\url{https://semanticlab.jrc.ec.europa.eu/} Last-accessed: March 2014}. The Semantic Web for Earth and Environmental Terminology (SWEET) \cite{sweet} OWL ontologies developed by NASA cover various aspects of environmental data from processes to phenomena involved in environmental observations. The AGROVOC vocabulary\footnote{\url{http://aims.fao.org/aos/agrovoc/} Last-accessed: March 2014}, that covers such topics as food, nutrition, agriculture and environment, and the Environmental Applications Reference Thesaurus(EARTh)\footnote{\url{http://datahub.io/dataset/environmental-applications-reference-thesaurus} Last-accessed: March 2014} were published in RDF.

In our work we didn't aim to make an exhaustive survey of the resources (e.g., vocabularies, ontologies, thesauri) that exist in the Earth Science domain to define domain specific concepts in RDF. Our goal is to provide a demonstration of how to re-use the existing resources to name entities of environmental observations. 

The following are the entities of the example observation:
\begin{itemize}
	\item[-] \textit{Feature of Interest}: air
	\item[-] \textit{Observed Property}: concentration of CO2
	\item[-] \textit{Procedure}: the UHEI flask sampler
	\item[-] \textit{Time}: 2010-01-01 at 00:00
	\item[-] \textit{Location}: (53$\circ$ 19'N, -9$\circ$ 54'W) at 25 meters above sea level
	\item[-] \textit{Result}: 391.318 
	\item[-] \textit{Unit of Measure}: parts per million
\end{itemize}

The following HTTP URIs were assigned to them:
\begin{itemize}
	\item[-] air - \href{http://www.eionet.europa.eu/gemet/concept/245}{gemet:245}
	\item[-] concentration of CO2 - \texttt{envri:co2-concentration}
	\item[-] the UHEI flask sampler - \texttt{envri:uhei-flask-sampler}
	\item[-] parts per million - \href{http://dbpedia.org/resource/Parts_per_million}{dbpedia:Parts\_per\_million}
\end{itemize}



\subsubsection*{Semantic Alignment}

The goal of the semantic alignment is to align the named entities within the ENVRI vocabulary:
\begin{itemize}
	\item[-] \href{http://www.eionet.europa.eu/gemet/concept/245}{gemet:245} \texttt{rdf:type} \texttt{envri:FeatureOfInterest}
	\item[-] \texttt{envri:co2-concentration} \texttt{rdf:type} \texttt{envri:ObservedProperty}
	\item[-] \texttt{envri:uhei-flask-sampler} \texttt{rdf:type} \texttt{envri:Procedure}
	\item[-] \href{http://dbpedia.org/resource/Parts_per_million}{dbpedia:Parts\_per\_million} \texttt{rdf:type} \texttt{envri:UnitOfMeasure}
\end{itemize}


\subsubsection*{Schematic Alignment}

The goal of the schematic alignment is to define the Data Cube data structure definition for the example observation. The structure is simply a set of properties needed to identify and interpret the observation. Figure~\ref{fig:icos-dsd} presents the data structure definition of the example observation.

\begin{figure}[h]
  \caption{The data structure definition of the example ICOS data set}\label{fig:icos-dsd}
  \centering
    \includegraphics[width=1\textwidth]{pics/icos-dsd}
\end{figure}



\subsection{Data Conversion}
To define mappings we used the Data Cube plug-in for Google Refine\footnote{\url{http://refine.deri.ie/qbExport} Last-accessed: March 2014}. Google Refine is a standalone desktop application. It runs as a web server and provides a GUI to work with the spreadsheet files formats. The Data Cube plug-in allows to define mappings from the cells in each records of the files to the required RDF view. The mappings were define for the data acquired during the data acquisition phase: one kind of mappings for the ICOS data and another kind of mappings for the Euro-Argo data. The mappings can be downloaded from \url{http://staff.science.uva.nl/~ttaraso1/html/resources/}.

\subsubsection*{Converted Example Observation}
Fig. ~\ref{fig:icos-dataset-example} represents a fragment of the example ICOS observations that contains measurements in parts per million of CO2 concentration in the air performed by the UHEI flask sampler. 

\begin{figure}[h]
  \caption{Example ICOS data set}\label{fig:icos-dataset-example}
  \centering
    \includegraphics[width=0.9\textwidth]{pics/icos-dataset-example}
\end{figure}

Note, that the observations were defined as the instance of the Data Cube class \texttt{qb:DataSet} and comply with the previously defined structure. Not all the properties were attached to the observations at the data set level, i.e., \texttt{envri:hasResult}, \texttt{envri:hasTime} and \texttt{envri:hasLocation} will define different values for different observations, thus, they have to be specified at the level of each observation. Figure~\ref{fig:icos-observation} illustrates the encoding of the example observation.

\begin{figure}[h]
  \caption{Example ICOS observation}\label{fig:icos-observation}
  \centering
    \includegraphics[width=0.9\textwidth]{pics/icos-observation}
\end{figure}



\subsection{Data Publication}

The RDF files were uploaded to the Virtuoso OSE RDF store\footnote{\url{http://virtuoso.openlinksw.com/dataspace/doc/dav/wiki/Main/} Last-accessed: March 2014} (the graph with the ICOS data - \texttt{http://example.envri.org/icos/} and the graph with the Euro-Argo data - \texttt{http://example.envri.org/euro-argo/}). The RDF dumps are available at \url{https://bitbucket.org/ttarasova/envri/} under the folder \texttt{study-case}.

The data can also be queried through a public SPARQL-endpoint at \url{http://data.politicalmashup.nl/sparql/}. Examples of SPARQL queries that were developed on top of the harmonised data are available at \url{http://staff.science.uva.nl/~ttaraso1/html/case-study.html#queries}. They were grounded in the ``Iceland Volcano Ash'' case study and aim to demonstrate the integration of the heterogeneous ICOS and Euro-Argo data based on the common terms of the ENVRI vocabulary and the Data Cube scheme.

\begin{description}
	\item[Q1] Retrieve all the observations for the days of the Volcano eruption (from 20 March to 23 June, 2010). 	
	\begin{itemize}
		\item[$\rightarrow$] common properties used: \texttt{hasFeatureOfInterestName}, \texttt{hasLocation}, \texttt{hasTime} and \texttt{hasUnitOfMesaureName}
	\end{itemize}

	\item[Q2] Retrieve the months in 2010 for which the data from the area next to the Iceland Volcano available.
	\begin{itemize}
		\item[$\rightarrow$] common properties used: \texttt{hasLocation} and \texttt{hasTime}
	\end{itemize}

	\item[Q3] What data providers have data for March 2010 in the area next to the Iceland Volcano?
	\begin{itemize}
		\item[$\rightarrow$] common properties used: \texttt{hasLocation} and \texttt{hasTime}
	\end{itemize}

	\item[Q4] What instruments were used to perform measurements in 2010 in the area next to the Iceland Volcano?
	\begin{itemize}
		\item[$\rightarrow$] common properties used: \texttt{hasProcedure}, \texttt{hasLocation} and \texttt{hasTime}
	\end{itemize}

\end{description}
