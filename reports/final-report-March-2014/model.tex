\section{Environmental Data Harmonisation Model}\label{sec:model}

In section~\ref{sec:ld-approach} we motivated the usage of Linked Data for environmental data integration. We argued that Linked Data addresses the technological and structural data heterogeneities, but leaves open two questions:

\begin{enumerate}
	\item \textit{How to resolve schematic data heterogeneity?}
	\item \textit{How to resolve semantic data heterogeneity?}
\end{enumerate}

In attempt to answer these questions, we built a harmonisation model for environmental data that:
\begin{enumerate}
	\item provides a common vocabulary to represent schema of environmental data in RDF;
	\item aligns semantics of environmental data through an intermediate ontological layer.
\end{enumerate} 

In this section we, first, present the key ideas of our model in Section~\ref{sec:intuition}. In Section~\ref{sec:concept-model} we describe the model in detail. The implementation of the model in RDF is discussed in Section~\ref{sec:vocab}. 

\subsection{Intuition}\label{sec:intuition}

Environmental data is diverse in nature. However, all environmental data share similar fundamental concepts. To identify these common concepts we rely on the ISO and OGC endorsed conceptual model for observations and measurements (OGC O\&M) \cite{OGC-OM} and the ontological analysis of the model given in \cite{probst}.

The central concept of the OGC O\&M model is \textbf{Observation}. Observation is understood as an event of measuring environmental phenomenon. Let's consider an example:

\vspace{3 mm}

\begin{addmargin}[2em]{2em}
	\textit{391.318 parts per million is the result of the observation of the concentration of CO2 in the atmosphere on 2010-01-01 at 00:00 made at (53$\circ$ 19'N, -9$\circ$ 54'W) at 25 meters above sea level using the UHEI flask sampler}
\end{addmargin}
\vspace{3 mm}

The key idea is that observation is interpreted through a set of observation's properties that encode the following information:
\begin{itemize}
	\item What the observation applies to? What is the \textbf{feature of interest}? (e.g., \textit{atmosphere})
	\item What phenomenon was observed? What is the \textbf{observed property} of the feature of interest? (e.g., \textit{concentration of CO2 in the atmosphere})
	\item How the observation was done? What is the \textbf{procedure}? (e.g., \textit{instrument UHEI flask sampler})
	\item What was the \textbf{result} of the observation? (e.g., \textit{391.318 parts per million})
	\item Where the observation happened? What is the \textbf{location} of the observation? (e.g., (53$\circ$ 19'N, -9$\circ$ 54'W) at 25 meters above sea level)
	\item When the observation happened? What is the observation \textbf{time}? (e.g., \textit{2010-01-01 at 00:00})
\end{itemize}

\subsection{Conceptual Model for Environmental Data}\label{sec:concept-model}

Figure~\ref{fig:model} summarises the core concepts of environmental data in an observation-centric model.

\begin{figure}[h]
  \caption{Environmental Data Model}\label{fig:model}
  \centering
    \includegraphics[width=0.8\textwidth]{pics/model}
\end{figure}


\begin{description}
	\item[Observation] is an event of measuring environmental phenomenon associated with a discrete time instant or period  and location. It involves application of a specified procedure, such as a sensor, instrument, algorithm or process chain. The procedure may be applied in-situ, remotely, or ex-situ with respect to the sampling location. The result of an observation is an estimate of the value of a property of some feature.
	\item[Feature of Interest] is the target of observation, e.g., an \textit{ocean} or \textit{atmosphere}.
	\item[Observed Property] is a property of the feature of interest being observed, e.g. \textit{CO2 concentration} of the atmosphere, \textit{temperature} of the ocean.
	\item[Procedure] is a procedure used to generate the result of the observation, e.g., \textit{instrument} that is used to sample air from the atmosphere.
	\item[Result] is an estimate of a value of the property of the feature of interest. For example, the {result of the observation of the concentration of carbon dioxide in the atmosphere is 391.318 parts per million}.
	\item[Location] is a geo(-spatial) location of the observation, e.g., samples of the air are taken \textit{25 meters above sea level}.
	\item[Time] is the observation time, e.g., the CO2 concentration of the atmosphere measured on 2010-03-10 at 13:00:00.
\end{description}


\subsubsection*{Comments}

\textit{Feature} is the core notion in all OGC specifications. The OGC specification \cite{OGC-OM} defines a feature as an abstraction of real world phenomena. The ontological analysis of the OGC O\&M model \cite{probst} revealed that OGC allowed two different interpretations of the feature:

\begin{itemize}
	\item as a geo-spatial real-world entity (e.g., \textit{the Atlantic ocean})
	\item as a pure information object that can represent either a real world entity or a category of real world entities
\end{itemize}

It is important to differentiate between these two interpretations, since the possible properties of the feature that can be observed in both cases will be different. For example, the width of a lake can be observed directly or it can be observed on a representation of a lake in a GIS. According to the first interpretation, the observed property (\textit{depth} of the Atlantic ocean) is associated with an identifiable real-world object (some ocean). Second interpretation suggests that only properties of an information object (e.g., a representation of the ocean in GIS) can be observed. 

In our model we follow the suggestion of \cite{probst} and consider a feature to be an information object that can represent a real-world entity (\textit{the Atlantic ocean}) or a category of real-world entities (\textit{ocean}).


\subsection{The ENVRI Vocabulary}\label{sec:vocab}

The ENVRI vocabulary is the RDFS\footnote{\url{http://www.w3.org/TR/rdf-schema/} Last-accessed: March 2014} formalisation of the model. A Web page describing vocabulary is available at \url{http://data.politicalmashup.nl/RDF/vocabularies/envri}. In addition to the HTML representation, the vocabulary is also available in RDF/XML at \url{http://data.politicalmashup.nl/RDF/vocabularies/envri.rdf} and Turtle at \url{http://data.politicalmashup.nl/RDF/vocabularies/envri.ttl}. 

The vocabulary contains several classes and properties. The following is the definition of the ranges of the properties (we use the prefixes defined in Appendix~\ref{app:prefixes}.):
\begin{itemize}
	\item \texttt{envri:hasTime} \texttt{rdfs:range} \texttt{time:TemporalEntity}
	\item \texttt{envri:hasLocation} \texttt{rdfs:range} \texttt{gsp:SpatialObject}
	\item \texttt{envri:hasFeatureOfInterest} \texttt{rdfs:range} \texttt{envri:FeatureOfInterest}
	\item \texttt{envri:hasProcedure} \texttt{rdfs:range} \texttt{envri:Procedure}
	\item \texttt{envri:hasObservedProperty} \texttt{rdfs:range} \texttt{envri:ObservedProperty}
\end{itemize}

Note the usage of two classes, \texttt{time:TemporalEntity} and \texttt{gsp:SpatialObject}, from the external ontologies:
\begin{itemize}
	\item{} \texttt{gsp:SpatialObject} is the class that is provided by the GeoSPARQL Ontology \cite{geosparql}. GeoSPARQL is defined by the Open Geospatial Consortium \cite{OGC} (OGC) as a core vocabulary for representing geospatial information in RDF.
	\item{} The class \texttt{time:TemporalEntity} of the Time Ontology in OWL \cite{time}. This ontology is developed by the Ontology Engineering and Patterns \cite{OEP} Task Force (OEP). The ontology provides an expressive vocabulary that can be used to represent both time intervals and time instants with different level of granularity up to seconds.
\end{itemize} 


\paragraph{Mappings from the ENVRI Vocabulary to Data Cube}

The RDF Data Cube vocabulary \cite{data-cube} is the W3C Recommendation to publish multi-dimensional data on the Web in such a way that it can be linked to related data sets and concepts. The Data Cube model was built on the cube model that underlies the Statistical Data and Metadata eXchange (SDMX)\footnote{SDMX initiative webpage \url{http://sdmx.org/?page_id=11} Last-accessed: March 2014}, an ISO standard for exchanging and sharing statistical data and metadata. 

Figure~\ref{fig:datacube} illustrates the core data model of the RDF Data Cube vocabulary.

\begin{figure}[h]
  \caption{Data Cube: Core Model}\label{fig:datacube}
  \centering
    \includegraphics[width=1\textwidth]{pics/datacube}
\end{figure}


\texttt{qb:Observation} belongs to \texttt{qb:DataSet} which has a particular structure defined by \texttt{qb:DataStructureDefinition}. The structure is composed by a set of \texttt{qb:ComponentProperty} specified by \texttt{qb:ComponentSpecification}. Data Cube differentiates between three kinds of properties:
\begin{itemize}
	\item \texttt{qb:DimensionProperty} is the class of component properties which represents the dimensions of the cube.
	\item \texttt{qb:AttributeProperty} is the class of component properties which represents attributes of observations in the cube, e.g. unit of measurement.
	\item \texttt{qb:MeasureProperty} is the class of component properties which represents the measured value of the phenomenon being observed.
\end{itemize}



The idea of using the Data Cube vocabulary for publishing environmental data is not new. We discussed the existing works in Section~\ref{sec:relwork}. What makes our work different from the existing ones is the idea of using an intermediate ontological layer, the ENVRI vocabulary, between the Data Cube and the domain specific terms. We defined mappings from the ENVRI vocabulary to Data Cube as follows:

\begin{itemize}
	\item \texttt{envri:hasResult} \texttt{rdfs:subPropertyOf} \texttt{qb:DimensionProperty}
	\item \texttt{envri:hasLocation} \texttt{rdfs:subPropertyOf} \texttt{qb:DimensionProperty}
	\item \texttt{envri:hasTime} \texttt{rdfs:subPropertyOf} \texttt{qb:DimensionProperty}
	\item \texttt{envri:hasFeatureOfInterest} \texttt{rdfs:subPropertyOf} \texttt{qb:AttributeProperty}
	\item \texttt{envri:hasProcedure} \texttt{rdfs:subPropertyOf} \texttt{qb:AttributeProperty}
	\item \texttt{envri:hasObservedProperty} \texttt{rdfs:subPropertyOf} \texttt{qb:MeasureProperty}
\end{itemize}
