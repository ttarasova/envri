\section{Conclusion}\label{sec:conclusion}

In this report we presented an observation-centric model for environmental data harmonisation. The model was based on the OGC and ISO endorsed conceptual model for ``Observations \& Measurements'' \cite{OGC-OM}. We discussed the formalisation of the model in RDFS, the ENVRI vocabulary. We also presented the workflow for harmonising and integrating heterogeneous environmental data which consists of four main processes: \textit{Data Acquisition}, \textit{Data Harmonisation}, \textit{Data Conversion} and \textit{Data Publication}. 

We discussed in detail the process of \textit{Data Harmonisation} and identified three steps: (1) \textit{Entity Naming}, (2) \textit{Semantic Alignment} and (3) \textit{Schematic Alignment}. The first step is analytical. It requires understanding of the data in question, as well as the existing resources that can be re-used for naming entities. \textit{Semantic Alignment} is about defining mappings from the named entities to the classes of the ENVRI vocabulary. The step of \textit{Schematic Alignment} concerns creation of the Data Cube \cite{data-cube} data structure definitions for the environmental data being harmonised.

We demonstrated the workflow on the ``Iceland Volcano Ash'' case study, in which we showed how to harmonise and integrate data from the two ENVRI research infrastructures: ICOS ATC and Eur-Argo. All the processes of the workflow were discussed and exemplified. Finally, we presented the SPARQL queries to the harmonised data. The queries were constructed using common terms of the ENVRI vocabulary and demonstrated integration of heterogeneous data.


\subsection{Discussions}

\subsubsection*{Environmental Data Harmonisation Model}

We present our observations on the usage of the harmonisation model for the purposes of the ``Iceland Volcano Ash'' case study.

\subsubsection*{Sampling Feature}

In the case study, we said that ICOS observations have feature of interest \textit{air}. However, we simplified our case study, as in practice, the observations are made on \textit{samples of air}, as the entire feature of interest, \textit{air} could not be exhaustively observed (cf. the \textit{weight of a specific animal} which is observed directly on the feature of interest, \textit{animal}).  

The OGC model for observations and measurements \cite{OGC-OM} defines the observation procedure that involves sampling in representative locations. The observations are then made on a \underline{sample of a feature of interest}, \textbf{Sampling Feature}, and the feature of interest is called a \textbf{Sampled Feature}. It is clear that for more rigorous modeling of environmental observations, this sampling model must be considered in the harmonisation model for environmental data.

\subsubsection*{Observed Property}

During the \textit{Entity Naming} step of data harmonisation, we could find the corresponding GEMET concept only for the observed property of the Euro-Argo observations, \href{http://www.eionet.europa.eu/gemet/concept/8366}{gemet:8366}, that represents the concept of \textit{temperature}. The observed property of the ICOS observations was named through a custom term, \texttt{envri-concept:co2-concentration}. This brings us to the discussion of different types of observed properties. The OGC O\&M \cite{OGC-OM} defines the following types of observed properties:


\begin{description}
	\item[Simple Properties] are single valued properties, e.g., \textit{temperature}, \textit{length} and \textit{power}.
	\item[Constrained Properties] properties qualify a feature of interest by adding constraints to the observed property. For example, \textit{surface-water-temperature} constraints the observed property (\textit{temperature}) on its depth, i.e., \textit{surface} implies the depth of the water to be 0 to 0.3 meters.
	\item[Compound Properties] are:
		\begin{itemize}
			\item \textbf{Composite Properties} are composed of an arbitrary set of properties. These properties may not be related to each other. For example, the observed property \textit{weather} is composed of a several properties: \textit{temperature}, \textit{rainfall}, \textit{wind velocity}, \textit{atmospheric pressure}, etc.
			\item \textbf{Series of Properties} are composed of homogeneous properties that differ by the value of one or more constraints. For example, spectral channels. \textit{Power} in 256 channels where \textit{wavelength} = 
			$0.9-1 \mu m, 1-1.1 \mu m, 1.1-1.2 \mu m,$ \dots
		\end{itemize}
\end{description}

The observed property, \textit{temperature} of the Euro-Argo observations is simple. As for the ICOS observations, more careful analysis is needed. It seems possible to say that the feature of interest being observed is \texttt{carbon dioxide} and not \texttt{air}\footnote{GEMET defines the corresponding URI for the concept of \textit{CO2}, \href{http://www.eionet.europa.eu/gemet/concept/1168}{gemet:1168}}. Then the observed property is \textit{concentration} and it is constrained on its media, \textit{air}. Note, that for \textit{concentration} GEMET also has the term defined \href{http://www.eionet.europa.eu/gemet/concept/1673}{gemet:1673}. 

This ICOS example shows us the difficulty in understanding environmental data. It is clear that expert participation will be required in order to properly analyse and annotate the concepts used to describe environmental observations. The proposed harmonisation model must also be properly extended to support these different types of observed properties. 

\subsubsection*{Procedure}

The ``Iceland Volcano Ash'' case study showed the difficulty in modelling the procedure of observations. This is because the procedure to obtain observations is fundamentally different in different Earth science communities and even in different research infrastructures within the same community. When modelling the ICOS observations, we said that the procedure was instrument. For Euro-Argo we said that the procedure was platform. Obviously, the procedure is more than just an instrument or platform.

Procedure in the OGC O\&M \cite{OGC-OM} refers to a description of such processing, that involves \textbf{Instrument(s)} hosted at a \textbf{Facility(ies)} on behalf of an \textbf{Activity}.

\begin{description}
	\item[Instrument] is a physical artifact which is used to make an observation, e.g., \textit{thermometer}. 
	\item[Facility] is a facility at/from which instruments are hosted or deployed to undertake environmental observation. Three types of facilities exist with respect to the location of observation:
	\begin{itemize}
		\item \textbf{In-situ Facilities}: when a sampling feature is co-located with the feature of interest, and the measurement process is performed at this location. An example of the in-situ facilities is an atmospheric station with a deployed instrument that continuously measures certain parameters of the atmosphere.
		\item \textbf{Ex-situ Facilities}: when the sampling feature is taken from the feature of interest and the measurement process is performed at a different location. For example, samples of air taken from the atmosphere and preserved in flasks to be analysed later at some other location.
		\item \textbf{Remote Facilities}: when the the feature of interest is observed remotely without sampling, and the measurement process is performed at a different from the feature location. An example of the remote sensing is Earth observation satellites.
	\end{itemize}
	\item[Activity] an activity that generates observations. Examples of observation activities include \textit{Monitoring Network}, \textit{Fixed Deployment}, \textit{Flight}, \textit{Cruise}, \textit{Field Trip}, \textit{Computation}, \textit{Targeted Programme of Monitoring}, etc.
\end{description}

More careful analysis is required to understand different kinds of procedures and ways to describe them. The open question is do we want to capture the semantics of these various procedures in our harmonisation model? Is it possible to align semantics of all different procedure in one common model? If the semantics are so different, does it make sense to try to align them in one model?

\subsubsection*{Fine-Grain Representation of Environmental Data}

The main advantage of Linked Data is that we can represent environmental data in a fine-grain way, i.e., up to a single observation. We believe that Data Cube brings interesting possibilities for data integration. For example, in the case study, the scientist was interested in data provided by several institutes \textit{during a specific time period}. The ICOS data is organised by atmospheric stations which perform measurements. In order to retrieve all relevant ICOS data, each data collection has to be examined. Similar situation was with the Euro-Argo observations that were provided in separate collections grouped according to the float that performed measurements. With Data Cube, we were able to automatically perform the task of retrieving and integrating measurements from \underline{any} collection of ICOS or Euro-Argo.

One of the drawback of such observation-centric representation is the amount of the transformed data it generates. This becomes impractical for those Earth science disciplines where data volume amounts to terabytes or petabytes of data (e.g., sensor data). To the best of our knowledge, there is no work related to the adoption of the RDF Data Cube vocabulary for such big amounts of data. Thus, a careful estimation of the resulting data sizes is needed, as well as survey of the existing technologies that can effectively manage this amount of data. The issue can also be addressed by reducing the amount of data to be transformed to Data Cube at the previous step of data discovery. Next, we explain in more detail the possible integration of the Linked Data Harmonisation component with the ENVRI data discovery portal.



\subsubsection*{Integration of Linked Data Harmonisation Component into WP4}\label{sec:ld-integration}

Together with Massimo Argenti and Roberto Cosu from ESA, task 4.1 WP4, we worked on possible directions on how to integrate our Linked Data Harmonisation Workflow (described in Section~\ref{sec:workflow}) into the context of WP4. The diagram in Fig.~\ref{fig:big-picture} illustrates the overall process of WP4. The diagram was taken from the report of the WP1 and WP4 meeting in Rome, 27 June 2013.


\begin{figure}[H]
  \caption{WP4: Overall Process}\label{fig:big-picture}
  \centering
    \includegraphics[width=0.8\textwidth]{pics/big-picture}
\end{figure} 

The key idea was to allow the data discovered through the ENVRI data discovery portal\footnote{\url{http://portal.envri.eu/} Last-accessed: March 2014} to be further harmonised, converted and integrated using the work presented in this document. More detailed presentation of the idea can be found in the presentation \textit{IntegrationOfLD.ppt} which is available at \url{https://bitbucket.org/ttarasova/envri} under the folder \\ \texttt{presentations/2013-Sept-Clermont-Ferrand/}. 

\paragraph{Prototype implementation}
Relevant to the ``Iceland Volcano Ash'' case study data sets were registered with the ENVRI data discovery portal and tagged with the tag ``Iceland Volcano''. The same data was acquired from the corresponding research infrastructures, harmonised, converted and stored in the Virtuoso OSE RDF store\footnote{The data can be accessed via the public SPARQL-endpoint from \url{http://data.politicalmashup.nl/sparql/}}. This tagged data can be discovered via the semantic search of the ENVRI portal\footnote{\url{http://portal.envri.eu/}}. The example SPARQL queries that perform data harmonisation and integration are presented at the bottom of the page. The prototype implementation is also discussed in the publication ``Semantically-Enabled Environmental Data Discovery and Integration: demonstration using the Iceland Volcano Use Case'' (Section~\ref{sec:publications}).

The works underlying the metadata harmonisation process by the task 4.1, and the data harmonisation process, presented in this report, were carried out separately from one another. The challenging task now is to align two harmonisation models to enable seamless connection between the data discovery and Linked Data Harmonisation components. This will allow to automatise the process of data conversion after the data discovery. The output of the Data Discovery component is a description of the data series (and data sets) that satisfy the user search. Our idea is to include additional information into this description, such information must uniquely identify the kind of mappings that has to be applied to the discovered data. The future work is to define an \underline{exact and sufficient} set of properties for each data type of each data provider, as well as to define the mappings for each data type. As soon as (and if) such information is identified, it can be added to the description of the data registered at the ENVRI portal. This will allow the data discovery component to pass the required information to the Linked Data Harmonisation component, so that the latter can execute the right mappings for the particular kind of discovered data. 




