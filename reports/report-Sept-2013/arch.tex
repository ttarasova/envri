\section{Architecture}\label{sec:arch}

The Linked Data component consists of the following sub-components: \textbf{Data Acquisition}, \textbf{Data Harmonization}, \textbf{Data Conversion} and \textbf{Data Publication}. Figure~\ref{fig:arch} illustrates the architecture of the component.

\begin{figure}[H]
  \caption{Architecture of the Linked Data Component}\label{fig:arch}
  \centering
    \includegraphics[width=0.8\textwidth]{pics/arch}
\end{figure}

Data Harmonization is the core of the LD component. It performs harmonization of data by defining mappings between the original data and the target view provided by the Environmental Data Harmonization model. I discuss the Data Harmonization component in detail in Section~\ref{sec:dh}. The goal of the Data Acquisition component is to collect and store original data from Research Infrastructure (RI). The goal of the Data Conversion component is to execute the mappings defined in the Data Harmonization component on the source data (obtained by the Data Acquisition component) and produce a harmonized data. The Data Publication component provides access to the transformed data.


\subsection{Data Harmonization}\label{sec:dh}
The core of the Data Harmonization component is the Environmental Data Harmonization Model. The Model provides a unified view over heterogeneous environmental data. 

\subsubsection*{Environmental Data Harmonization Model}
Figure~\ref{fig:model} illustrates the observation-centric model for environmental data harmonization.

\begin{figure}[h]
  \caption{Environmental Data Model}\label{fig:model}
  \centering
    \includegraphics[width=0.8\textwidth]{pics/model}
\end{figure}


\paragraph{The RDF Data Cube vocabulary}
The RDF Data Cube vocabulary \cite{data-cube} is the W3C candidate recommendation for encoding multi-dimensional data on the Web using RDF. Data Cube provides generic terms to capture structural semantics of multi-dimensional data, as well as means to extend the vocabulary with the domain specific terms. I defined the ENVRI vocabulary to extend Data Cube with generic common terms of environmental observations.

\paragraph{The ENVRI vocabulary}
In order to define the ENVRI vocabulary, I considered the OGC and ISO standard conceptual model for observations and measurements \cite{OGC-OM}. I also considered the ontological analysis of the model presented in \cite{probst}. The current version of the ENVRI vocabulary is published at \url{http://data.politicalmashup.nl/RDF/vocabularies/envri}. The vocabulary is under development and can be modified. The classes and properties of the vocabulary are listed at \url{http://staff.science.uva.nl/~ttaraso1/html/envri.html#vocab}.

\paragraph{Cross-Domain Ontologies}
The ENVRI vocabulary re-uses the following existing cross-domain ontologies for better data re-usability:
\begin{itemize}
	\item The Time Ontology in OWL\footnote{\url{http://www.w3.org/TR/owl-time/}} is an ontology of temporal concepts.
	\item The GeoSPARQL vocabulary\footnote{\url{http://www.opengeospatial.org/standards/geosparql}}. GeoSPARQL is defined by the Open Geospatial Consortium (OGC) \cite{OGC} as a core vocabulary for representing geospatial information in RDF. 
	\item The Semantic Web for Earth and Environmental Terminology (SWEET) ontologies \footnote{\url{http://sweet.jpl.nasa.gov/}}
\end{itemize}


\subsubsection*{Mappings' Definition}
For each data product of each environmental data provider (i.e., source data) mappings must be defined from the source data to the Data Harmonization Model.
The process of defining mappings from source environmental data to the target view provided by the Data Harmonization Model is depicted in Figure~\ref{fig:process}.

\begin{figure}[H]
  \caption{Mappings' Definition}\label{fig:process}
  \centering
    \includegraphics[width=0.3\textwidth]{pics/process}
\end{figure}

\paragraph{Entity Naming}
\begin{sloppypar}
Entity naming is a process of assigning HTTP URIs to the concepts of interest. For this, the metadata of the obtained data is analyzed and the concepts of interest are identified. The existing Earth Science vocabularies are examined for having corresponding concepts that can be used to represent the concepts of interest. In case when there was no suitable concept found among the terms of the Earth Science vocabularies, a custom HTTP URI is defined within the following path \texttt{http://example.envri.org/vocab/concept/\{concept-name\}}. 
\end{sloppypar}

\paragraph{Entity Typing}
The named entities from the previous step are further aligned within the ENVRI vocabulary. This is done by typing the named entities as instances of the corresponding ENVRI classes.

