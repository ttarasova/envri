Linked Data Harmonisation component for environmental data
===

This repository accumulates the results of my research activities within the scope of the EU FP7 ENVRI project http://envri.eu/ 

Research highlights
---

   * In my work I investigated opportunities of Linked Data for harmonising heterogeneous environmental data, i.e., making the data compatible, comparable and interoperable.
   
   * The main contribution of my work is the observation-centric harmonisation model for environmental data built on two standards: the [ISO and OGC endorsed conceptual model of observations and measurements](http://portal.opengeospatial.org/files/?artifact_id=41579), and the [W3C RDF Data Cube vocabulary](http://www.w3.org/TR/vocab-data-cube/) that provided a machinery to describe Earth observations in RDF.

   * The harmonisation model was implemented in the ENVRI vocabulary which is published [here](http://data.politicalmashup.nl/RDF/vocabularies/envri)
   
   * [this page](http://staff.science.uva.nl/~ttaraso1/html/envri.html#casestudy) presents the Iceland Volcano ash case study to demonstrate and evaluate the proposed harmonisation model
      
   * *final-report-March-2014* explains in detail the research ideas, the harmonisation model and its demonstration on the Iceland Volcano Ash case study.


The content of the repository
===

   **reports**
   
   ---
   
   The folder contains **all** the reports that were produced for the project.
     
   
   * *final-report-March-2014* is the main and final report that explains the research ideas and their implementation and evaluation on the Iceland Volcano case study.
   
   Other reports were added for sake of completeness. They chronologically represent the development of the research ideas:
     
   * *WP4.2-status-december-2012* contains the report with the intiail ideas of applying the Linked Data principles to harmonise environmental data.
   
   * *report-may-2013* contains the report with the initial results of implementing the research ideas: the first version of the ENVRI vocabulary.
   
   * *report-Sept-2013* describes the observation-centric data model and a Linked Data harmonisation component.
     
   
   **presentations**
   
   ---

   The folder contains all the presentations produced for the project's meetings.
   
   

   **publications**
   
   ---
   
   Scientific publications produced during the project:
   
   * *bersys-2013* contains the paper "ParlBench: a SPARQL-benchmark for electronic publishing applications" that was published together with Maarten Marx in the [post-proceedings book of the Extended Semantic Web conference](http://link.springer.com/chapter/10.1007%2F978-3-642-41242-4_2). In the work we studied performances of RDF store systems on large RDF datasets. The paper can be accessed online at [http://ceur-ws.org/Vol-981/BeRSys2013paper2.pdf](http://ceur-ws.org/Vol-981/BeRSys2013paper2.pdf)
   
   * *kesw-2013* contains the paper "Semantically-Enabled Environmental Data Discovery and Integration: demonstration using the Iceland Volcano Use Case". The work was done in collaboration with the European Space Agency and explains the approach to integrate the Linked Data harmonisation component with the OpenSearch based data discovery platform
     
     The demo is accessible online at [http://portal.envri.eu/](http://portal.envri.eu/)


   **html**
   
   ---
   
   This folder contains the web pages:

   * *envri.html* describes the Environmental Data Harmonisation Model
   * *case-study.html* presents demonstration of the model on the "Iceland Volcano Ash" study case
   
   Folders with various resources:
   
   * *data* contains original datasets used for the "Iceland Volcano Ash" study case
   * *documentation* the final ENVRI report and the pre-printed copy of the KESW 2013 paper
   * *pics* figures and diagrams used on the webpages
   * *queries* SPARQL queries used for the "Iceland Volcano Ash" study case
   * *resources* GoogleRefine projects and mappings used for the "Iceland Volcano Ash" study case
   * *scripts* python scripts that were used to apply the mappings to the original datasets programmatically

      
